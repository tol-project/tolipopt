/* JournalTol.hpp: API TOL ipopt

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#ifndef _JournalTol_

#define _JournalTol_

#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include <tol/tol_bout.h>

#include <coin/IpTNLP.hpp>
#include <coin/IpIpoptApplication.hpp>
#include <coin/IpJournalist.hpp>

namespace Ipopt
{
  /** TolJournal class. This is a particular Journal implementation that
   *  writes to Tol standard output.
   */
  class TolJournal : public Journal
  {
     
  public:
    /** Constructor. */
    TolJournal(const std::string& name, EJournalLevel default_level);

    /** Destructor. */
    virtual ~TolJournal()
    {}


  protected:
    /**@name Implementation version of Print methods - Overloaded from
     * Journal base class.
     */
    //@{
    /** Print to the designated output location */
    void PrintImpl(EJournalCategory category, EJournalLevel level,
                           const char* str);

    /** Printf to the designated output location */
    void PrintfImpl(EJournalCategory category, EJournalLevel level,
                            const char* pformat, va_list ap);

    /** Flush output buffer.*/
    void FlushBufferImpl();
    //@}

  private:
    /**@name Default Compiler Generated Methods
     * (Hidden to avoid implicit creation/calling).
     * These methods are not implemented and 
     * we do not want the compiler to implement
     * them for us, so we declare them private
     * and do not define them. This ensures that
     * they will not be implicitly created/called. */
    //@{
    /** Default Constructor */
    TolJournal();

    /** Copy Constructor */
    TolJournal(const TolJournal&);

    /** Overloaded Equals Operator */
    void operator=(const TolJournal&);
    //@}

    /** buffer for sprintf.  Being generous in size here... */
    BText buffer_;
  };
}
#endif /*_JournalTol_*/
