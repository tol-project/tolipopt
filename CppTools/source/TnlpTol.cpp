/* TnlpTol.cpp: API TOL ipopt

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#include "TnlpTol.hpp"
#include "JournalTol.hpp"

#include <tol/tol_blanguag.h>

BText TnlpTol::_MID_nlp("[TolIpopt::Nlp]");
int TnlpTol::badFields_ = false;
SmartPtr<Journal> TnlpTol::jrnl;

/////////////////////////////////////////////////////////////////////////////
union TnlpTolBDat
/////////////////////////////////////////////////////////////////////////////
{
  SmartPtr<TnlpTol> *ptr_;
  double   hnd_; 
};

/////////////////////////////////////////////////////////////////////////////
int TnlpTol::InitClass()
/////////////////////////////////////////////////////////////////////////////
{
  jrnl = new TolJournal ("console", J_ITERSUMMARY);
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
BDat TnlpTol::code_addr( SmartPtr<TnlpTol>& ptr )
/////////////////////////////////////////////////////////////////////////////
{
  TnlpTolBDat convert;
  convert.ptr_ = &ptr;
  return convert.hnd_;
}

/////////////////////////////////////////////////////////////////////////////
SmartPtr<TnlpTol>& TnlpTol::decode_addr( BDat& hnd )
/////////////////////////////////////////////////////////////////////////////
{
  TnlpTolBDat convert;
  convert.hnd_ = hnd.Value();
  return *(convert.ptr_);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::check_is_finite(BVMat& v, const BText& name)
/////////////////////////////////////////////////////////////////////////////
{
  int nf = v.IsFinite().Not().Sum();
  if(nf==0) { return (true); }
  else
  {
    Error(MID()+" Member VMatrix "+name+" has "+nf+" unknown or infinite values\n");
    badFields_ = true;
    return(false);
  }
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::check_is_finite(BDat& v, const BText& name)
/////////////////////////////////////////////////////////////////////////////
{
  int nf = !(v.IsFinite());
  if(nf==0) { return (true); }
  else
  {
    Error(MID()+" Member Real "+name+"="+v+" is not a known finite value\n");
    badFields_ = true;
    return(false);
  }
}

/////////////////////////////////////////////////////////////////////////////
BSyntaxObject* TnlpTol::EnsureMember(const BText& name)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE TnlpTol::EnsureMember(")<<name<<") 1");
  BSyntaxObject* mbm = wrapper_->Member(name);
//Std(BText("\nTRACE TnlpTol::EnsureMember(")<<name<<") 2");
  if(!mbm)
  {
    Error(BText(MID()+"Unexpected meber name "+name));
    badFields_ = true;
    BGrammar::Turn_StopFlag_On(); 
  };
  if(mbm->Grammar()==GraVMatrix())
  {
    check_is_finite(VMat(mbm),name);
  }
/*
  Real _.id_status = ?;
  Real _.obj_value = ?;
  Real _.handler = ?;

  else if(mbm->Grammar()==GraReal())
  {
    check_is_finite(Dat(mbm),name);
  }
*/
  return(mbm);
};



/////////////////////////////////////////////////////////////////////////////
TnlpTol::TnlpTol(BNameBlock& wrapper)
/////////////////////////////////////////////////////////////////////////////
: 
  isGood_(true),
  wrapper_(&wrapper),
  n_(&Dat(EnsureMember("n"))),
  x_lower(&VMat(EnsureMember("x_lower"))),
  x0(&VMat(EnsureMember("x0"))),
  x_upper(&VMat(EnsureMember("x_upper"))),
  _id_status(&Dat(EnsureMember("_.id_status"))),
  _co_status(&Text(EnsureMember("_.co_status"))),
  _x(&VMat(EnsureMember("_.x"))),
  _z_L(&VMat(EnsureMember("_.z_L"))),
  _z_U(&VMat(EnsureMember("_.z_U"))),
  _g(&VMat(EnsureMember("_.g"))),
  _lambda(&VMat(EnsureMember("_.lambda"))),
  _obj_value(&Dat(EnsureMember("_.obj_value"))),
  _handler(&Dat(EnsureMember("_.handler")))
{
//Std(BText("\nTRACE")+" TnlpTol::TnlpTol 1 this='"<<((int)this)<<"'\n");
  if(badFields_) 
  { 
    badFields_ = false;
    isGood_ = false;
    Error(MID()+" Error during creation of TnlpTol!\n");
    return; 
  }
//Std(BText("\nTRACE")+" TnlpTol::TnlpTol 2\n");
  app = IpoptApplicationFactory();
//Std(BText("\nTRACE")+" TnlpTol::TnlpTol 3\n");
  app->Jnlst()->DeleteAllJournals();
//Std(BText("\nTRACE")+" TnlpTol::TnlpTol 4\n");
  app->Jnlst()->AddJournal(jrnl);
//Std(BText("\nTRACE")+" TnlpTol::TnlpTol 5\n");
  set_status(NlpNotInitialized);
}


/////////////////////////////////////////////////////////////////////////////
/** Default destructor */
TnlpTol::~TnlpTol()
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::~TnlpTol 1 this='"<<((int)this)<<"'\n");
}


/////////////////////////////////////////////////////////////////////////////
/** Intialize the IpoptApplication and process the options */
int TnlpTol::initialize()
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::initialize 1\n");
  // Intialize the IpoptApplication and process the options
  ApplicationReturnStatus status;
  status = app->Initialize();
  if (status != Solve_Succeeded) 
  {
    isGood_ = false;
    Error(MID()+" Error during initialization!\n");
  }
  return (int) status;
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return some info about the nlp */
bool TnlpTol::check_x_bounds(Index n)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::check_x_bounds 1 n="+n+"\n");
  bool ok = true;
  if(!n)
  {
    Error(MID()+" Number of variables must be at least one.");
    ok = false;
  }
  if(!x_lower->Rows())
  {
    x_lower->BlasRDense(n,1,BDat::NegInf());
  }
  if(!x_upper->Rows())
  {
    x_upper->BlasRDense(n,1,BDat::PosInf());
  }
  if((x_lower->Rows()!=n)|| (x_lower->Columns()!=1))
  {
    Error(MID()+" x_lower matrix should be ("+n+"x"+1+") instead of ("+
          x_lower->Rows()+"x"+x_lower->Columns()+")");
    ok = false;
  }
  if((x_upper->Rows()!=n)|| (x_upper->Columns()!=1))
  {
    Error(MID()+" x_upper matrix should be ("+n+"x"+1+") instead of ("+
          x_upper->Rows()+"x"+x_upper->Columns()+")");
    ok = false;
  }
  return(ok);
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return the x-bounds for my problem */
bool TnlpTol::get_x_bounds_info(Index n, Number* x_l, Number* x_u)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::get_x_bounds_info 1 n="+n+"\n");
  for (Index i=0; i<n; i++) {
    x_l[i] = x_lower->GetCell(i,0);
    x_u[i] = x_upper->GetCell(i,0);
  }
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return the starting point for the algorithm */
bool TnlpTol::get_starting_point(Index n, bool init_x, Number* x,
                                 bool init_z, Number* z_L, Number* z_U,
                                 Index m, bool init_lambda,
                                 Number* lambda)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::get_starting_point 1 n="+n+"\n");
  if (!init_x || init_z || init_lambda) {
    return false;
  }
  if(x0->Rows() && ((x0->Rows()!=n)|| (x0->Columns()!=1)))
  {
    
    BText msg = MID()+" Initial values x0 matrix should be ("+n+"x"+1+") "
      "instead of ("+x0->Rows()+"x"+x0->Columns()+") but it's not necessary "
      "use them.";
    if(!init_x) { Warning(msg); } 
    else { Error(msg); }
    x0->Delete();
  }
  if(init_x && ((x0->Rows()!=n)|| (x0->Columns()!=1)))
  {
    x0->BlasRDense(n,1,0.0);
  } 
  // set the starting point
  for (Index i=0; i<n; i++) {
    x[i] = x0->GetCell(i,0);
  }
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::set_dense(int n, const Number* x, BVMat& xd)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::set_dense 1 n="+n+"\n");
  xd.BlasRDense(n,1);
  double* x_;
  int nzmax;
  xd.StoredData(x_, nzmax);
  if(nzmax!=n) 
  { 
    Error(MID()+" set_dense "+nzmax+"=nzmax!=n="+n);
    return(false); 
  }
  memcpy(x_, x, n*sizeof(double));
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::get_dense(int n, Number* x, const BVMat& xd)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::get_dense 1 n="+n+"\n");
  const double* x_;
  int nzmax;
  if(xd.Code()==BVMat::ESC_blasRdense)
  {
    xd.StoredData(x_, nzmax);
    if(nzmax!=n) 
    { 
      Error(MID()+" get_dense "+nzmax+"=nzmax!=n="+n);
      return(false); 
    }
    memcpy(x, x_, n*sizeof(double));
  }
  else
  {
    BVMat dense;
    dense.Convert(xd,BVMat::ESC_blasRdense);
    return(get_dense(n,x,dense));
  }
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::get_triplet_structure(int nn, Index* iRow, Index* jCol, const BMatrix<double>& ijx)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::get_triplet_structure 1 nn="+nn+"\n");
  if(ijx.Rows()!=nn)
  {
    Error(MID()+" get_triplet_structure "+ijx.Rows()+"=ijx.Rows()!=nn="+nn);
    return(false);
  }    
  Index k;
  for(k=0; k<nn; k++)
  {
    iRow[k] = (int)ijx(k,0)-1;
    jCol[k] = (int)ijx(k,1)-1;
  //Std(BText("\nTRACE")+" TnlpTol::get_triplet_structure 2.1 k="+k+" i="+iRow[k]+" j="+iCol[k]+")\n");
  }
//Std(BText("\nTRACE")+" TnlpTol::get_triplet_structure 2\n");
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::get_triplet_values(int nn, Number* values, const BMatrix<double>& ijx, Number obj_factor)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::get_triplet_values 1 nn="+nn+"\n");
  if(ijx.Rows()!=nn)
  {
    Error(MID()+" get_triplet_values "+ijx.Rows()+"=ijx.Rows()!=nn="+nn);
    return(false);
  }    
  Index k;
  for(k=0; k<nn; k++)
  {
    values[k] = ijx(k,2)*obj_factor;
  //Std(BText("\nTRACE")+" TnlpTol::get_triplet_values 2.1 x["+k+":("+(int)(ijx(k,0)-1)+","+(int)(ijx(k,1)-1)+")]="+values[k]+"\n");
  }
//Std(BText("\nTRACE")+" TnlpTol::get_triplet_values 3\n");
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::get_triplet_structure(int nn, Index* iRow, Index* jCol, const BVMat& t)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::get_triplet_structure 1 nn="+nn+"\n");
  if(t.Code()==BVMat::ESC_chlmRtriplet)
  {
    BMatrix<double> ijx;
    t.GetTriplet(ijx);
    return(get_triplet_structure(nn, iRow, jCol, ijx));
  }
  else if(t.Rows())
  {
    BVMat triplet;
    triplet.Convert(t,BVMat::ESC_chlmRtriplet);
    return(get_triplet_structure(nn,iRow,jCol,triplet));
  }
//Std(BText("\nTRACE")+" TnlpTol::get_triplet_structure 2\n");
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::get_triplet_values(int nn, Number* values, const BVMat& t, Number obj_factor)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::get_triplet_values 1 nn="+nn+"\n");
  if(t.Code()==BVMat::ESC_chlmRtriplet)
  {
  //Std(BText("\nTRACE")+" TnlpTol::get_triplet_values 2\n");
    BMatrix<double> ijx;
    t.GetTriplet(ijx);
    return(get_triplet_values(nn, values, ijx, obj_factor));
  }
  else if(t.Rows())
  {
  //Std(BText("\nTRACE")+" TnlpTol::get_triplet_values 4\n");
    BVMat triplet;
    triplet.Convert(t,BVMat::ESC_chlmRtriplet);
  //Std(BText("\nTRACE")+" TnlpTol::get_triplet_values 5\n");
    return(get_triplet_values(nn,values,triplet,obj_factor));
  }
  return(false);
}


/////////////////////////////////////////////////////////////////////////////
/** This method is called when the algorithm is complete so the TNLP can store/write the solution */
void TnlpTol::set_status(int status)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::set_status 1 status="+status+"\n");
  *_id_status = status;
  switch(status)
  {
    case Solve_Succeeded : *_co_status = "Solve_Succeeded"; break;
    case Solved_To_Acceptable_Level : *_co_status = "Solved_To_Acceptable_Level"; break;
    case Infeasible_Problem_Detected : *_co_status = "Infeasible_Problem_Detected"; break;
    case Search_Direction_Becomes_Too_Small : *_co_status = "Search_Direction_Becomes_Too_Small"; break;
    case Diverging_Iterates : *_co_status = "Diverging_Iterates"; break;
    case User_Requested_Stop : *_co_status = "User_Requested_Stop"; break;
    case Feasible_Point_Found : *_co_status = "Feasible_Point_Found"; break;
    case Maximum_Iterations_Exceeded : *_co_status = "Maximum_Iterations_Exceeded"; break;
    case Restoration_Failed : *_co_status = "Restoration_Failed"; break;
    case Error_In_Step_Computation : *_co_status = "Error_In_Step_Computation"; break;
    case Maximum_CpuTime_Exceeded : *_co_status = "Maximum_CpuTime_Exceeded"; break;
    case Not_Enough_Degrees_Of_Freedom : *_co_status = "Not_Enough_Degrees_Of_Freedom"; break;
    case Invalid_Problem_Definition : *_co_status = "Invalid_Problem_Definition"; break;
    case Invalid_Option : *_co_status = "Invalid_Option"; break;
    case Invalid_Number_Detected : *_co_status = "Invalid_Number_Detected"; break;
    case Unrecoverable_Exception : *_co_status = "Unrecoverable_Exception"; break;
    case NonIpopt_Exception_Thrown : *_co_status = "NonIpopt_Exception_Thrown"; break;
    case Insufficient_Memory : *_co_status = "Insufficient_Memory"; break;
    case Internal_Error : *_co_status = "Internal_Error"; break;
    case NlpInitialized : *_co_status = "Initialized"; break;
    case NlpNotInitialized : *_co_status = "Not_Initialized"; break;
    default : *_co_status = "unknown"; 
  }
//Std(BText("\nTRACE")+" TnlpTol::set_status 2 _co_status="+_co_status+"\n");
}

/////////////////////////////////////////////////////////////////////////////
/** This method is called when the algorithm is complete so the TNLP can store/write the solution */
void TnlpTol::finalize_solution(SolverReturn status,
                                Index n, const Number* x, const Number* z_L, const Number* z_U,
                                Index m, const Number* g, const Number* lambda,
                                Number obj_value,
              				          const IpoptData* ip_data,
			                          IpoptCalculatedQuantities* ip_cq)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::finalize_solution 1\n");
  set_dense(n, x, *_x);
  set_dense(n, z_L, *_z_L);
  set_dense(n, z_U, *_z_U);
  set_dense(m, g, *_g);
  set_dense(m, lambda, *_lambda);
  double _obj_value = obj_value;
/*
  set_status(status);
  if(status=SUCCESS) { _co_status="SUCCESS"; }
  if(status=MAXITER_EXCEEDED) { _co_status="MAXITER_EXCEEDED"; }
  if(status=CPUTIME_EXCEEDED) { _co_status="CPUTIME_EXCEEDED"; }
  if(status=STOP_AT_TINY_STEP) { _co_status="STOP_AT_TINY_STEP"; }
  if(status=STOP_AT_ACCEPTABLE_POINT) { _co_status="STOP_AT_ACCEPTABLE_POINT"; }
  if(status=LOCAL_INFEASIBILITY) { _co_status="LOCAL_INFEASIBILITY"; }
  if(status=USER_REQUESTED_STOP) { _co_status="USER_REQUESTED_STOP"; }
  if(status=FEASIBLE_POINT_FOUND) { _co_status="FEASIBLE_POINT_FOUND"; }
  if(status=DIVERGING_ITERATES) { _co_status="DIVERGING_ITERATES"; }
  if(status=RESTORATION_FAILURE) { _co_status="RESTORATION_FAILURE"; }
  if(status=ERROR_IN_STEP_COMPUTATION) { _co_status="ERROR_IN_STEP_COMPUTATION"; }
  if(status=INVALID_NUMBER_DETECTED) { _co_status="INVALID_NUMBER_DETECTED"; }
  if(status=TOO_FEW_DEGREES_OF_FREEDOM) { _co_status="TOO_FEW_DEGREES_OF_FREEDOM"; }
  if(status=INVALID_OPTION) { _co_status="INVALID_OPTION"; }
  if(status=OUT_OF_MEMORY) { _co_status="OUT_OF_MEMORY"; }
  if(status=INTERNAL_ERROR) { _co_status="INTERNAL_ERROR"; }
  Std(BText("\n")+MID()+"\tFinal status="+_co_status+"\tobj_value="+obj_value);
*/
}

/*

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::intermediate_callback(AlgorithmMode mode,
                                    Index iter, Number obj_value,
                                    Number inf_pr, Number inf_du,
                                    Number mu, Number d_norm,
                                    Number regularization_size,
                                    Number alpha_du, Number alpha_pr,
                                    Index ls_trials,
                                    const IpoptData* ip_data,
                                    IpoptCalculatedQuantities* ip_cq)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE")+" TnlpTol::intermediate_callback 1\n");
  BText phase = "";
//Std(BText("\nTRACE")+" TnlpTol::intermediate_callback 2\n");
  if(mode==RegularMode)          { phase = "Regular"; }
//Std(BText("\nTRACE")+" TnlpTol::intermediate_callback 3\n");
  if(mode==RestorationPhaseMode) { phase = "Restoration"; }
//Std(BText("\nTRACE")+" TnlpTol::intermediate_callback 4\n");
//Std(BText("\n")+MID()+"\tphase:"+phase+ "\titer="+iter+"\tobj_value="+obj_value);
//Std(BText("\nTRACE")+" TnlpTol::intermediate_callback 5\n");
  if(BGrammar::StopFlag()) { return(false); }
//Std(BText("\nTRACE")+" TnlpTol::intermediate_callback 6\n");
  return(true);
}
*/


