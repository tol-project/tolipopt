/* CppTools.cpp: API C++ entre TOL y el paquete nlopt (non linear optimization)
   M�s detalles del paquete en http://ab-initio.mit.edu/wiki/index.php/NLopt

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */


//Starts local namebock scope
#define LOCAL_NAMEBLOCK _local_namebtntLock_

#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bcommon.h>
#include <tol/tol_boper.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_bdatgra.h>
#include <tol/tol_btxtgra.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_bcodgra.h>
#include <tol/tol_blanguag.h>
#include <tol/tolCint.h>

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& _local_namebtntLock_ = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function 
DynAPI void* GetDynLibNameBlockTolIpopt()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

#define ERR(cond,msg,ret) \
if(cond) { \
  Error(_MID<<msg); \
  return(ret); \
}


#include "TnlpTol.hpp"
#include "TnlpTolSqp.hpp"

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_create_TolTnlpSqp);
DefMethod(1, BDat_create_TolTnlpSqp, 
  "sqp.create", 1, 1, "NameBlock",
  "(NameBlock problem)",
  "Creates the internal C++ instance of the IPOPT solver of type "
  "TolTnlpSqp that handles with Sparse Quadratic Problems.\n",
  BOperClassify::MatrixAlgebra_);
void BDat_create_TolTnlpSqp::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolIpopt::CppTools::sqp.create] ";
  BUserNameBlock* unb = UNameBlock(Arg(1));
  BNameBlock& nb = unb->Contens();
  TnlpTolSqp* sqp = new TnlpTolSqp(nb);
  contens_ = sqp->isGood_;
}

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_nlp_initialize);
DefMethod(1, BDat_nlp_initialize, 
  "nlp.initialize", 1, 1, "Real",
  "(Real handler)",
  "Initialize the IPOPT application.\n",
  BOperClassify::MatrixAlgebra_);
void BDat_nlp_initialize::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolIpopt::CppTools::nlp.initialize] ";
  BDat& _handler = Dat(Arg(1));
  TnlpTol* nlp = TnlpTol::decode_addr(_handler);
  if(nlp && nlp->isGood_)
  {
    contens_ = nlp->initialize();
  }
  else
  {
    contens_ = BDat::Unknown();
  }
}

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_nlp_optimize);
DefMethod(1, BDat_nlp_optimize, 
  "nlp.optimize", 1, 1, "Real",
  "(Real handler)",
  "Optimizes.\n",
  BOperClassify::MatrixAlgebra_);
void BDat_nlp_optimize::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolIpopt::CppTools::nlp.optimize] ";
  BDat& _handler = Dat(Arg(1));
  TnlpTol* nlp = TnlpTol::decode_addr(_handler);
  if(nlp && nlp->isGood_)
  {
    contens_ = nlp->app->OptimizeTNLP(nlp);
  }
  else
  {
    contens_ = BDat::Unknown();
  }
}

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_destroy_tnlp);
DefMethod(1, BDat_destroy_tnlp, 
  "nlp.destroy", 1, 1, "Real",
  "(Real handler)",
  "Destroys the internal C++ instance of an IPOPT solver\n",
  BOperClassify::MatrixAlgebra_);
void BDat_destroy_tnlp::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolIpopt::CppTools::nlp.destroy] ";
  BDat& _handler = Dat(Arg(1));
  TnlpTol* tnlp = TnlpTol::decode_addr(_handler);
  if(tnlp)
  {
    delete tnlp;
    contens_ = true;
  }
  else
  {
    contens_ = false;
  }
}

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_options_set_string);
DefMethod(1, BDat_options_set_string,
  "options.setString", 3, 3, 
  "Real Text Text",
  "(Real handler, Text name, Text value)",
  "Sets an string option of IPOPT",
  BOperClassify::MatrixAlgebra_);
void BDat_options_set_string::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolIpopt::CppTools::options.setString] ";
  BDat& _handler = Dat(Arg(1));
  TnlpTol* tnlp = TnlpTol::decode_addr(_handler);
  if(tnlp)
  {
    BText& name = Text(Arg(2));
    BText& value = Text(Arg(3));
    contens_ = tnlp->app->Options()->SetStringValue(name.String(), value.String());
  }
  else
  {
    contens_ = false;
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_options_set_numeric);
DefMethod(1, BDat_options_set_numeric, 
  "options.setNumeric", 3, 3, "Real Text Real",
  "(Real handler, Text name, Real value)",
  "Sets a real valued option of IPOPT",
  BOperClassify::MatrixAlgebra_);
void BDat_options_set_numeric::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolIpopt::CppTools::options.setNumeric] ";
  BDat& _handler = Dat(Arg(1));
  TnlpTol* tnlp = TnlpTol::decode_addr(_handler);
  if(tnlp)
  {
    BText& name = Text(Arg(2));
    BDat& value = Dat(Arg(3));
    contens_ = tnlp->app->Options()->SetNumericValue(name.String(), value.Value());
  }
  else
  {
    contens_ = false;
  }
};

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_options_set_integer);
DefMethod(1, BDat_options_set_integer, 
  "options.setInteger", 3, 3, "Real Text Real",
  "(Real handler, Text name, Real value)",
  "Sets an integer valued option of IPOPT."
  "Valid options are described at "
  "http://www.coin-or.org/Ipopt/documentation/node59.html#app.options_ref\n"
  ""
  "Please, note that all options are lower-case\n",
  BOperClassify::MatrixAlgebra_);
void BDat_options_set_integer::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolIpopt::CppTools::options.setInteger] ";
  BDat& _handler = Dat(Arg(1));
  TnlpTol* tnlp = TnlpTol::decode_addr(_handler);
  if(tnlp)
  {
    BText& name = Text(Arg(2));
    BDat& value = Dat(Arg(3));
    contens_ = tnlp->app->Options()->SetIntegerValue(name.String(), (int)value.Value());
  }
  else
  {
    contens_ = false;
  }
};


/* */