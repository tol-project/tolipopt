/* TnlpTolSqp.hpp: API TOL ipopt
   M�s detalles del paquete en http://ab-initio.mit.edu/wiki/index.php/NLopt

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#include "TnlpTolSqp.hpp"

BText TnlpTolSqp::_MID_sqp("[TolIpopt::Sqp]");

/////////////////////////////////////////////////////////////////////////////
TnlpTolSqp::TnlpTolSqp(BNameBlock& wrapper)
/////////////////////////////////////////////////////////////////////////////
: 
  TnlpTol(wrapper_),
  C(VMat(EnsureMember("C"))),
  c(VMat(EnsureMember("c"))),
  c0(Dat(EnsureMember("c0"))),
  A_lower(VMat(EnsureMember("A_lower"))),
  A(VMat(EnsureMember("A"))),
  A_upper(VMat(EnsureMember("A_upper")))
{
  if(badFields_) 
  { 
    badFields_ = false;
    isGood_ = false;
    Error(MID()+" Error during creation of TnlpTolSqp!\n");
    return; 
  }
  C_triplet.Convert(C,BVMat::ESC_chlmRtriplet);
  if(A.Rows()) { A_triplet.Convert(A,BVMat::ESC_chlmRtriplet); }
}


/////////////////////////////////////////////////////////////////////////////
/** Default destructor */
TnlpTolSqp::~TnlpTolSqp()
/////////////////////////////////////////////////////////////////////////////
{}
;

/////////////////////////////////////////////////////////////////////////////
/** Method to return some info about the nlp */
bool TnlpTolSqp::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                              Index& nnz_h_lag, IndexStyleEnum& index_style)
/////////////////////////////////////////////////////////////////////////////
{
  n = C.Columns();
  m = A.Rows();
  nnz_jac_g = A.NonNullCells();
  nnz_h_lag = C.NonNullCells();
  bool ok = true;
  if(!n)
  {
    Error(MID()+" C matrix cannot be empty");
    ok = false;
  }
  if(C.Columns()!=n)
  {
    Error(MID()+" C matrix should be ("+n+"x"+n+") instead of ("+C.Rows()+"x"+C.Columns()+")");
    ok = false;
  }
  if(m && (A.Columns()!=n))
  {
    Error(MID()+" A matrix should be ("+m+"x"+n+") instead of ("+A.Rows()+"x"+A.Columns()+")");
    ok = false;
  }
  if(m && ((A_lower.Rows()!=m)|| (A_lower.Columns()!=1)))
  {
    Error(MID()+" A_lower matrix should be ("+m+"x"+1+") instead of ("+A_lower.Rows()+"x"+A_lower.Columns()+")");
    ok = false;
  }
  if(m && ((A_upper.Rows()!=m)|| (A_upper.Columns()!=1)))
  {
    Error(MID()+" upper matrix should be ("+m+"x"+1+") instead of ("+A_upper.Rows()+"x"+A_upper.Columns()+")");
    ok = false;
  }
  ok = ok && check_x_bounds(n);

  // use the C style numbering of matrix indices (starting at 0)
  index_style = TNLP::C_STYLE;

  return(ok);
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return the bounds for my problem */
bool TnlpTolSqp::get_bounds_info(Index n, Number* x_l, Number* x_u,
                                 Index m, Number* g_l, Number* g_u)
/////////////////////////////////////////////////////////////////////////////
{
  get_x_bounds_info(n,x_l,x_u);
  for (Index i=0; i<m; i++) {
    g_l[i] = A_lower.GetCell(i,0);
    g_u[i] = A_upper.GetCell(i,0);
  }
  return(true);    
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return the objective value */
bool TnlpTolSqp::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
/////////////////////////////////////////////////////////////////////////////
{
  BVMat xd, xt;
  set_dense(n,x,xd);
  xt = xd.T();
  BVMat f = xt*C*xd + xt*c + c0.Value();
  obj_value = f.GetCell(0,0);
  return true;
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return the gradient of the objective */
bool TnlpTolSqp::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
/////////////////////////////////////////////////////////////////////////////
{
  BVMat xd;
  set_dense(n,x,xd);
  BVMat grad_f_ = C*xd + c;
  get_dense(n,grad_f,grad_f_);
  return true;
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return the constraint residuals */
bool TnlpTolSqp::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
/////////////////////////////////////////////////////////////////////////////
{
  BVMat xd;
  set_dense(n,x,xd);
  BVMat g_ = A*xd;
  get_dense(n,g,g_);
  return true;
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return:
 *   1) The structure of the jacobian (if "values" is NULL)
 *   2) The values of the jacobian (if "values" is not NULL)
 */
bool TnlpTolSqp::eval_jac_g(Index n, const Number* x, bool new_x,
                            Index m, Index nele_jac, Index* iRow, Index *jCol,
                            Number* values)
/////////////////////////////////////////////////////////////////////////////
{
  if(!values)
  {
    return(get_triplet_structure(nele_jac, iRow, jCol, A_triplet));
  } 
  else
  {
    return(get_triplet_values(nele_jac, values, A_triplet));
  }
};

/////////////////////////////////////////////////////////////////////////////
/** Method to return:
 *   1) The structure of the hessian of the lagrangian (if "values" is NULL)
 *   2) The values of the hessian of the lagrangian (if "values" is not NULL)
 */
bool TnlpTolSqp::eval_h(Index n, const Number* x, bool new_x,
                        Number obj_factor, Index m, const Number* lambda,
                        bool new_lambda, Index nele_hess, Index* iRow,
                        Index* jCol, Number* values)
/////////////////////////////////////////////////////////////////////////////
{
  if(!values)
  {
    return(get_triplet_structure(nele_hess, iRow, jCol, C_triplet));
  } 
  else
  {
    return(get_triplet_values(nele_hess, values, C_triplet*obj_factor));
  }
};
