/* TnlpTol.hpp: API TOL ipopt

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#ifndef _TnlpTol_

#define _TnlpTol_

#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include <tol/tol_bdatgra.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_btxtgra.h>
#include <tol/tol_bvmatgra.h>
#include <tol/tol_bnameblock.h>

#include <coin/IpTNLP.hpp>
#include <coin/IpIpoptApplication.hpp>

using namespace Ipopt;


///////////////////////////////////////////////////////////////////////////////
class TnlpTol : public TNLP
///////////////////////////////////////////////////////////////////////////////
{
protected:
  static BText _MID_nlp;
  static const BText& MID() { return(_MID_nlp); }

public:
  static int badFields_;

  //TOL wrapper
  BNameBlock& wrapper_;

  //Input data
  const BDat& n_;
  const BVMat& x_lower;
  const BVMat& x0;
  const BVMat& x_upper;

  //Output data
  BDat& _id_status;
  BText& _co_status;
  BVMat& _x;
  BVMat& _z_L;
  BVMat& _z_U;
  BVMat& _g;
  BVMat& _lambda;
  BDat& _obj_value;

  //Auxiliar data
  bool isGood_;
  BDat& _handler;
  SmartPtr<IpoptApplication> app;

  TnlpTol(BNameBlock& wrapper);
  virtual ~TnlpTol();
  static BDat code_addr( TnlpTol* ptr );
  static TnlpTol* decode_addr( BDat& addr );

  BSyntaxObject* EnsureMember(const BText& name);

  int initialize();
  bool check_x_bounds(Index n);
  bool get_x_bounds_info(Index n, Number* x_l, Number* x_u);
  bool get_starting_point(Index n, bool init_x, Number* x,
                          bool init_z, Number* z_L, Number* z_U,
                          Index m, bool init_lambda,
                          Number* lambda);
  static bool set_dense(int n, const Number* x, BVMat& xd);
  static bool get_dense(int n, Number* x, const BVMat& xd);
  static bool get_triplet_structure(int nn, Index* iRow, Index* jCol, const BVMat& t);
  static bool get_triplet_values(int nn, Number* x, const BVMat& t);
  void finalize_solution(SolverReturn status,
                         Index n, const Number* x, const Number* z_L, const Number* z_U,
                         Index m, const Number* g, const Number* lambda,
                         Number obj_value,
                				 const IpoptData* ip_data,
				                 IpoptCalculatedQuantities* ip_cq);
  bool intermediate_callback(AlgorithmMode mode,
                                   Index iter, Number obj_value,
                                   Number inf_pr, Number inf_du,
                                   Number mu, Number d_norm,
                                   Number regularization_size,
                                   Number alpha_du, Number alpha_pr,
                                   Index ls_trials,
                                   const IpoptData* ip_data,
                                   IpoptCalculatedQuantities* ip_cq);
};

#endif /*_TnlpTol_*/
