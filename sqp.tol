///////////////////////////////////////////////////////////////////////////////
// FILE   : problem.tol
// PURPOSE: Class TolIpopt::@Problem
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.SQP = "Class to handle with Sparse Quadratic "
"Problems:\n"
"\n"
"       T        T          \n"
"  min x  C x + c  x + c0   \n"
"  subject to               \n"
"    l_A <= A x <= u_A      \n"
"    l_x <=   x <= u_x      \n"
"\n"
"  If C and A are sparse matrices methods will be more efficient.\n";
Class @SQP : @NLP
///////////////////////////////////////////////////////////////////////////////
{
  VMatrix C;
  VMatrix c;
  Real    c0 = 0;
  VMatrix A_lower = Rand(0,0,0,0);
  VMatrix A = Rand(0,0,0,0);
  VMatrix A_upper = Rand(0,0,0,0);

  /////////////////////////////////////////////////////////////////////////////
  Real create(Real void)
  /////////////////////////////////////////////////////////////////////////////
  {
    TolIpopt::CppTools::sqp.create(_this)
  };

/*
  /////////////////////////////////////////////////////////////////////////////
  Real __destroy (Real void)
  /////////////////////////////////////////////////////////////////////////////
  {
    destroy (void)
  };
*/

  /////////////////////////////////////////////////////////////////////////////
  Real build_and_optimize(Real void)
  /////////////////////////////////////////////////////////////////////////////
  {
    Real create(?);
    Real set.option.list(_.options);
    Real initialize(?);
    Real optimize(?)
  };

  /////////////////////////////////////////////////////////////////////////////
  Static @SQP Optimize.Unbounded(
    Set options, 
    VMatrix C_, VMatrix c_, Real c0_)
  /////////////////////////////////////////////////////////////////////////////
  {
    TolIpopt::@SQP sqp = 
    [[
      Real    n = VRows(C_);
      VMatrix C = C_;
      VMatrix c = c_;
      Real    c0 = c0_;
      Set _.options = DeepCopy(options)
    ]];
    Real sqp::build_and_optimize(?);
    sqp
  };

  /////////////////////////////////////////////////////////////////////////////
  Static @SQP Optimize.Bounded(
    Set options, 
    VMatrix C_, VMatrix c_, Real c0_,
    VMatrix x_lower_, VMatrix x_upper_)
  /////////////////////////////////////////////////////////////////////////////
  {
    TolIpopt::@SQP sqp = 
    [[
      Real    n = VRows(C_);
      VMatrix C = C_;
      VMatrix c = c_;
      Real    c0 = c0_;
      VMatrix x_lower = x_lower_;
      VMatrix x_upper = x_upper_;
      Set _.options = DeepCopy(options)
    ]];
    Real sqp::build_and_optimize(?);
    sqp
  };

  /////////////////////////////////////////////////////////////////////////////
  Static @SQP Optimize.Constrained(
    Set options, 
    VMatrix C_, VMatrix c_, Real c0_,
    VMatrix x_lower_, VMatrix x_upper_,
    VMatrix A_lower_, VMatrix A_, VMatrix A_upper_)
  /////////////////////////////////////////////////////////////////////////////
  {
    TolIpopt::@SQP sqp = 
    [[
      Real    n = VRows(C_);
      VMatrix C = C_;
      VMatrix c = c_;
      Real    c0 = c0_;
      VMatrix x_lower = x_lower_;
      VMatrix x_upper = x_upper_;
      VMatrix A_lower = A_lower_;
      VMatrix A = A_;
      VMatrix A_upper = A_upper_;
      Set _.options = DeepCopy(options)
    ]];
    Real sqp::build_and_optimize(?);
    sqp
  }

};


///////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.SMRS = "Class to handle with Sparse Minimum Residuals Solve "
"Problems:\n"
"\n"
"       T                 \n"
"  min e  e               \n"
"  subject to             \n"
"    e = v - V x          \n"
"    l_A <= A x <= u_A    \n"
"    l_x <=   x <= u_x    \n"
"\n"
"  If X and A are sparse matrices methods will be more efficient.\n";
Class @SMRS : @SQP
///////////////////////////////////////////////////////////////////////////////
{
  Real m;
  VMatrix V; //Input matrix
  VMatrix v; //Output matrix
  VMatrix C = Rand(0,0,0,0);
  VMatrix c = Rand(0,0,0,0);

  /////////////////////////////////////////////////////////////////////////////
  Real set_quadratic(Real void)
  /////////////////////////////////////////////////////////////////////////////
  {
    VMatrix C  := MtMSqr(V)*1/m;
    VMatrix c  := -Tra(Tra(v)*V)*(2/m);
    Real    c0 := VMatDat(MtMSqr(v),1,1)*(1/m);
    True
  };

  /////////////////////////////////////////////////////////////////////////////
  Real change_X(VMatrix X)
  /////////////////////////////////////////////////////////////////////////////
  {
    VMatrix V  := X;
    VMatrix C  := MtMSqr(V)*1/m;
    VMatrix c  := -Tra(Tra(v)*V)*(2/m);
    True
  };

  /////////////////////////////////////////////////////////////////////////////
  Real change_y(VMatrix y)
  /////////////////////////////////////////////////////////////////////////////
  {
    VMatrix v  := y;
    VMatrix c  := -Tra(Tra(v)*V)*(2/m);
    Real    c0 := VMatDat(MtMSqr(v),1,1)*(1/m);
    True
  };

  /////////////////////////////////////////////////////////////////////////////
  Static @SMRS Create(Real n_, Real m_)
  /////////////////////////////////////////////////////////////////////////////
  {
    TolIpopt::@SMRS sqp = 
    [[
      Real    n = n_;
      Real    m = m_;
      VMatrix V = Constant(m,n,0);
      VMatrix v = Constant(m,1,0)
    ]];
    Real sqp::create(?);
    sqp
  };

  /////////////////////////////////////////////////////////////////////////////
  Static @SMRS Optimize.Unbounded(
    Set options, 
    VMatrix X, VMatrix y)
  /////////////////////////////////////////////////////////////////////////////
  {
    TolIpopt::@SMRS sqp = 
    [[
      Real    n = VColumns(X);
      Real    m = VRows(X);
      VMatrix V = X;
      VMatrix v = y;
      Set _.options = DeepCopy(options)
    ]];
    Real sqp::set_quadratic(?);
    Real sqp::build_and_optimize(?);
    sqp
  };

  /////////////////////////////////////////////////////////////////////////////
  Static @SMRS Optimize.Bounded(
    Set options, 
    VMatrix X, VMatrix y,
    VMatrix x_lower_, VMatrix x_upper_)
  /////////////////////////////////////////////////////////////////////////////
  {
    TolIpopt::@SMRS sqp = 
    [[
      Real    n = VColumns(X);
      Real    m = VRows(X);
      VMatrix V = X;
      VMatrix v = y;
      VMatrix x_lower = x_lower_;
      VMatrix x_upper = x_upper_;
      Set _.options = DeepCopy(options)
    ]];
    Real sqp::set_quadratic(?);
    Real sqp::build_and_optimize(?);
    sqp
  };

  /////////////////////////////////////////////////////////////////////////////
  Static @SMRS Optimize.Constrained(
    Set options, 
    VMatrix X, VMatrix y,
    VMatrix x_lower_, VMatrix x_upper_,
    VMatrix A_lower_, VMatrix A_, VMatrix A_upper_)
  /////////////////////////////////////////////////////////////////////////////
  {
    TolIpopt::@SMRS sqp = 
    [[
      Real    n = VColumns(X);
      Real    m = VRows(X);
      VMatrix V = X;
      VMatrix v = y;
      VMatrix x_lower = x_lower_;
      VMatrix x_upper = x_upper_;
      VMatrix A_lower = A_lower_;
      VMatrix A = A_;
      VMatrix A_upper = A_upper_;
      Set _.options = DeepCopy(options)
    ]];
    Real sqp::set_quadratic(?);
    Real sqp::build_and_optimize(?);
    sqp
  }

};




