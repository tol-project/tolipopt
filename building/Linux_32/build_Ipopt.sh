#!/bin/sh

IPOPT_DWN_DIR=$(dirname $0)/Ipopt
BUILD_DIR=$IPOPT_DWN_DIR/build
IPOPT_DIR=/usr/local/Ipopt

echo -n "mkdir? (y/n) "
read answer
if [ "$answer" = "y" ]; then
  if [ -d "$BUILD_DIR" ]; then 
    rm -r "$BUILD_DIR"
  fi
  mkdir -p "$BUILD_DIR"
fi


cd $BUILD_DIR

echo -n "configure? (y/n) "
read answer
if [ "$answer" = "y" ]; then
  ../configure --prefix=$IPOPT_DIR
fi

echo -n "make? (y/n) "
read answer
if [ "$answer" = "y" ]; then
  make
fi

echo -n "test? (y/n) "
read answer
if [ "$answer" = "y" ]; then
  make test
fi

echo -n "install? (y/n) "
read answer
if [ "$answer" = "y" ]; then
  sudo make install
fi

echo -n "press enter to continue "
read answer

