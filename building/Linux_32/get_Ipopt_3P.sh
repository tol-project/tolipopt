#!/bin/sh

cd $(dirname $0)

echo -n "Blas? (y/n) "
read answer
if [ "$answer" = "y" ]; then
  cd Ipopt/ThirdParty/Blas
  ./get.Blas
  cd ../../..
fi

echo -n "Lapack? (y/n) "
read answer
if [ "$answer" = "y" ]; then
  cd Ipopt/ThirdParty/Lapack
  ./get.Lapack
  cd ../../..
fi

echo -n "MUMPS? (y/n) "
read answer
if [ "$answer" = "y" ]; then
  cd Ipopt/ThirdParty/Mumps
  ./get.Mumps
  cd ../../..
fi

echo -n "Metis? (y/n) "
read answer
if [ "$answer" = "y" ]; then
  cd Ipopt/ThirdParty/Metis
  ./get.Metis
  cd ../../..
fi

echo -n "ASL? (y/n) "
read answer
if [ "$answer" = "y" ]; then
  cd Ipopt/ThirdParty/ASL
  ./get.ASL
  cd ../../..
fi

echo -n "press enter to continue "
read answer


